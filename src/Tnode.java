import java.util.*;

public class Tnode {

    private String name;
    private Tnode firstChild;
    private Tnode nextSibling;

    public Tnode(String s) {
        name = s;
    }
    public Tnode getFirstChild() {
        return firstChild;
    }
    public Tnode getNextSibling() {
        return nextSibling;
    }
    public String getName() {
        return name;
    }
    @Override
    public String toString() {
        return firstChild == null ? String.valueOf(name) :
                (name + "(" + firstChild + "," + nextSibling + ")");
    }

    public static Tnode buildFromRPN (String pol) {
//	   if (pol.contains("[a-zA-Z]+")) {
//	   throw new RuntimeException(" incorrect number/operator " + pol);
//	   }
//    need to do through boolean?
//if (pol.length() <=4){
//	throw new NoSuchElementException(" Not enough elements  " + pol);
//}
        boolean igor = pol.matches(".*[a-zA-Z]+.*");
        if(igor==true){
            throw new NoSuchElementException(" incorrect number/operator " + pol);
        }

        Stack<Tnode> stack = new Stack<Tnode>();

        String[] arr = pol.split(" ");

        for (String s : arr) {
            Tnode node = new Tnode(s);
            if ("+-*/".indexOf(s) != -1) {
                node.nextSibling = stack.pop();
                node.firstChild = stack.pop();
            }
            stack.push(node);
        }

        if (stack.size() > 1) {
            throw new RuntimeException("Too many numbers were entered and not enough operators: " + pol);
        }
        if (stack.isEmpty()==true) {
            throw new RuntimeException("No elements" + " start|" +  pol + "|end");
        }

        return stack.pop();
    }

    public static void main (String[] param) {
        String rpn = "1 2 3 * +";
        System.out.println ("RPN: " + rpn);
        Tnode res = buildFromRPN (rpn);
        System.out.println ("Tree: " + res);
    }
}
//http://stackoverflow.com/questions/19895308/converting-from-reverse-polish-notationrpn-into-a-tree-form

//https://www.youtube.com/watch?v=TrfcJCulsF4